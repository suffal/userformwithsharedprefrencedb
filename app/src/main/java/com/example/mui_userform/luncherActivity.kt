package com.example.mui_userform

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.*
import androidx.lifecycle.ViewModelProvider
import com.example.mui_userform.R.layout.activity_luncher
import com.example.mui_userform.Reposetory.sharedpreRepository
import com.example.mui_userform.Viewmodel.sharedprefrancerepoviewmodel
import com.example.mui_userform.databinding.ActivityLuncherBinding
import com.example.mui_userform.factory.sharedprefrancerepositoryviewmodelfactory

class luncherActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding:ActivityLuncherBinding
    lateinit var viewmodelFactory: sharedprefrancerepositoryviewmodelfactory
    lateinit var viewmodel: sharedprefrancerepoviewmodel

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        binding=DataBindingUtil.setContentView(this,R.layout.activity_luncher)
        binding.btnRagister.setOnClickListener(this)
        binding.skip1.setOnClickListener(this)
        binding.textviewragister.setOnClickListener(this)





        viewmodelFactory =  sharedprefrancerepositoryviewmodelfactory(sharedpreRepository,this)
        viewmodel = ViewModelProvider(this, viewmodelFactory)[sharedprefrancerepoviewmodel::class.java]

    }

    override fun onClick(p0: View?) {
        when(p0?.id){

            R.id.textviewragister->{
                val intent= Intent(this,signinActivity::class.java)
                startActivity(intent)

            }

            R.id.skip1->{
                val intent= Intent(this,MainActivity::class.java)
                startActivity(intent)

            }


            R.id.btn_ragister->{



                if (binding.lFirstName.editableText!!.isEmpty()&&binding.lLastName.editableText!!.isEmpty()

                    && binding.lMobileNo.editableText!!.isEmpty()){
                    Toast.makeText(this,"please fill required value", Toast.LENGTH_SHORT).show()
                }
                else if(binding.lFirstName.editableText!!.isEmpty()){
                    Toast.makeText(this,"Please enter Firstname",Toast.LENGTH_SHORT).show()
                    binding.lFirstName.requestFocus()

                }

                else if(binding.lLastName.editableText!!.isEmpty()){
                    Toast.makeText(this,"Please enter last name",Toast.LENGTH_SHORT).show()
                    binding.lLastName.requestFocus()

                }
                else if(binding.lMobileNo.editableText!!.isEmpty()){
                    Toast.makeText(this,"Please enter Phone number",Toast.LENGTH_SHORT).show()
                    binding.lMobileNo.requestFocus()

               }
                else{

                     Toast.makeText(this, "successfully registered", Toast.LENGTH_LONG).show()
                    viewmodel.saveData(binding.lFirstName.text.toString(), binding.lLastName.text.toString(),binding.lMobileNo.text.toString())
                    startActivity(Intent(this,MainActivity::class.java))

                }



            }
        }

    }


}