package com.example.mui_userform

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.setContentView
import androidx.lifecycle.ViewModelProvider
import com.example.mui_userform.R.layout
import com.example.mui_userform.Reposetory.sharedpreRepository
import com.example.mui_userform.Viewmodel.sharedprefrancerepoviewmodel
import com.example.mui_userform.databinding.ActivitySigninBinding
import com.example.mui_userform.factory.sharedprefrancerepositoryviewmodelfactory

class signinActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding:ActivitySigninBinding
    lateinit var viewmodelFactory: sharedprefrancerepositoryviewmodelfactory
    lateinit var viewmodel: sharedprefrancerepoviewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_signin)


        binding.skip2.setOnClickListener(this)
        binding.btnLogin.setOnClickListener(this)
          binding.textviewlogin.setOnClickListener(this)

        viewmodelFactory =  sharedprefrancerepositoryviewmodelfactory(sharedpreRepository,this)
        viewmodel = ViewModelProvider(this, viewmodelFactory)[sharedprefrancerepoviewmodel::class.java]





    }

    override fun onClick(p0: View?) {
        when(p0?.id){
         R.id.skip2->{

             val intent= Intent(this,MainActivity::class.java)
             startActivity(intent)

         }


            R.id.textviewlogin ->{

                val intent= Intent(this,luncherActivity::class.java)
                startActivity(intent)
            }
            R.id.btn_login->{


                if(binding.SMobileNo.editableText.toString().equals(viewmodel.getphone())){
                    startActivity(Intent(this, MainActivity::class.java))
                }else {
                    Toast.makeText(this, "${binding.SMobileNo.editableText.toString()} not registered", Toast.LENGTH_SHORT).show()
                }






            }


        }

        }
}