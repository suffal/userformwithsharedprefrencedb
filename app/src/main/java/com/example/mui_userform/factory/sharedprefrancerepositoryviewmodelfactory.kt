package com.example.mui_userform.factory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mui_userform.Reposetory.sharedpreRepository
import com.example.mui_userform.Viewmodel.sharedprefrancerepoviewmodel


class sharedprefrancerepositoryviewmodelfactory(private val sharedPrefrepo: sharedpreRepository,private val context: Context):ViewModelProvider.Factory {



    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(sharedprefrancerepoviewmodel::class.java)){
            return sharedprefrancerepoviewmodel(sharedPrefrepo, context) as T
        }
        throw IllegalArgumentException("Unknown class")
    }



}